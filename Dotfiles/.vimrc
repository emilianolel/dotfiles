"Configuración general de vim

filetype on
set number "Nos permite enumerar las líneas del archivo. 
set relativenumber "Nos dice la posición relativa al cursor.
syntax on "nos muestra de diferentes colores las palabras que contenga nuestro archivo, dependiendo de la sintaxis del mismo. Esto es muy útil a la hora de escribir código.
set softtabstop=4 
set shiftwidth=4
set tabstop=4
set autoindent
set smartindent
colorscheme deep-space
set wildmode=longest,list
set wildmenu

set spell spelllang=es,en
highlight LineNr ctermfg=blue
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=02

set timeout ttimeout         " separate mapping and keycode timeouts
set timeoutlen=250           " mapping timeout 250ms  (adjust for preference)
set ttimeoutlen=15           " keycode timeout 15ms
inoremap <Space><Space> <Esc>/<++><Enter>"_c4l
inoremap ¿ ¿?<Space><++><Esc>F?i
inoremap ¡ ¡!<Space><++><Esc>F!i

autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

map! <C-A> <ESC>"ayy"aPi\begin{<Del><ESC>Ea}<ESC><CR>i\end{<Del><ESC>Ea}<ESC>O

if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    " autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.vim/plug.vim')

Plug 'sirver/ultisnips'
    let g:UltiSnipsExpandTrigger = '<tab>'
    let g:UltiSnipsJumpForwardTrigger = '<tab>'
    let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

Plug 'lervag/vimtex'
    let g:tex_flavor='latex'
    let g:vimtex_view_method='zathura'
    let g:vimtex_quickfix_mode=0

Plug 'KeitaNakamura/tex-conceal.vim'
    set conceallevel=1
    let g:tex_conceal='abdmg'
    hi Conceal ctermbg=none

call plug#end() " start all the plugins above
