#!/bin/bash

vol=`amixer | grep "Front Left: Playback" | grep -E -o '[0-9][0-9]?%'`

[ ${vol%?} -eq 0  ] && [ ${#vol} -eq 3 ] && echo " 100%" && echo ""
[ ${vol%?} -eq 0  ] && [ ${#vol} -eq 2 ] && echo " $vol" && echo ""
[ ${vol%?} -le 50 ] && [ ${vol%?} -gt 0 ] && echo " $vol" && echo ""
[ ${vol%?} -lt 100 ] && [ ${vol%?} -gt 50 ] && echo " $vol" && echo ""
