#!/bin/bash

dyh=`date '+%Y-%m-%d %H:%M'`

a=${dyh:0:4}
m=${dyh:5:2}
d=${dyh:8:2}

echo " $d/$m/$a"
