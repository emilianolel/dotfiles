#!/bin/bash

[[ $BLOCK_BUTTON = "1" ]] && alacritty --hold -e bash ~/.config/i3blocks/.si3blocks/conectar.sh

nmcli dev wifi > ~/.config/i3blocks/.si3blocks/auxwifi.txt

nom=`awk 'BEGIN{FS=" "}{if ($1=="*"){print $3}}' ~/.config/i3blocks/.si3blocks/auxwifi.txt`
bar=`awk 'BEGIN{FS=" "}{if ($1=="*"){print $9}}' ~/.config/i3blocks/.si3blocks/auxwifi.txt`
int=`awk 'BEGIN{FS=" "}{if ($1=="*"){print $8}}' ~/.config/i3blocks/.si3blocks/auxwifi.txt`

echo "$bar $nom"
echo "$bar"

[ $int -le 100 ] && [ $int -gt 80 ] && echo "#26A69A"
[ $int -le 80 ] && [ $int -gt 60 ] && echo "#D4E157"
[ $int -le 60 ] && [ $int -gt 40 ] && echo "#FFEB3B"
[ $int -le 40 ] && [ $int -gt 20 ] && echo "#C0392B"
[ $int -le 20 ] && [ $int -gt 0 ] && echo "#2196F3"
