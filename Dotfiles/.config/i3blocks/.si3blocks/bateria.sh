#!/bin/bash

BAT=$(acpi -b | grep -E -o '[0-9][0-9]?%')
STAT=`acpi -b | awk '{print $3}'`

[ "${BAT%?}" == "00" ] && echo " 100%  " && echo "  " && echo "#2196F3"
[ "$STAT" == "Charging," ] || [ "$STAT" == "Unknown," ] && echo " $BAT  " && echo "  " && echo "#2196F3"
[ "$STAT" == "Discharging," ] && [ ${BAT%?} -le 05 ] && [ ${BAT%?} -gt 00 ] && echo " $BAT  " && echo "  " && echo "#FF0000"
[ "$STAT" == "Discharging," ] && [ ${BAT%?} -le 25 ] && [ ${BAT%?} -gt 05 ] && echo " $BAT  " && echo "  " && echo "#FF8000"
[ "$STAT" == "Discharging," ] && [ ${BAT%?} -le 50 ] && [ ${BAT%?} -gt 25 ] && echo " $BAT  " && echo "  " && echo "#2196F3"
[ "$STAT" == "Discharging," ] && [ ${BAT%?} -le 75 ] && [ ${BAT%?} -gt 50 ] && echo " $BAT  " && echo "  " && echo "#2196F3"
[ "$STAT" == "Discharging," ] && [ ${BAT%?} -lt 100 ]  && [ ${BAT%?} -gt 75 ] && echo " $BAT  " && echo "  " && echo "#2196F3"
