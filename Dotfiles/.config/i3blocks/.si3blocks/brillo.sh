#!/bin/bash

while read BLOCK_BUTTON
do
	if [[ $BLOCK_BUTTON == "1" ]]
	then
		a=`cat /sys/class/backlight/intel_backlight/brightness`
		brillo=`expr $a - 5000`
	elif [[ $BLOCK_BUTTON == "3" ]]
	then
		a=`cat /sys/class/backlight/intel_backlight/brightness`
		brillo=`expr $a + 5000`
	fi
	if [[ $brillo -lt 5000 ]]
	then 
		brillo=5000
	else
		echo "$brillo" > /sys/class/backlight/intel_backlight/brightness
	fi
done
